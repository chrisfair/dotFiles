# I3 Shortcut Guide

sMOD = Shifted mod on my ergodox it is the key the center thumb key on the right side and MOD is
the center thumb key on the keft 


## Borders
* **MOD u** ... Set the window border to none
* **MOD y** ... Set the border to 1 pixel
* **MOD n** ... Set the border to default size


## Application Shortcuts
* **MOD  RETURN** ... Terminal
* **sMOD  q** ... Kill a window
* **MEH r** ... Mouse config
* **MEH 1** ... web browser
* **MEH c** ... VS Code
* **MEH s** ... Steam
* **MEH t** ... Teams
* **HYPER p** ... Password-Gorilla
* **HYPER h** ... Help (this file)
* **HYPER k** ... Keyboard Layout
* **MEH v** ... Keyboard Layout
* **PRINT** ... Screenshot
* **MEH p** ... Soundsettings
* **HYPER j** ... Joplin-desktop

## Change Focus
* **MOD j** ... focus left
* **MOD k** ... focus down
* **MOD l** ... focus up
* **MOD ;** ... focus right

## Move Focused Window
* **sMOD j** ... focus left
* **sMOD k** ... focus down
* **sMOD l** ... focus up
* **sMOD ;** ... focust right



