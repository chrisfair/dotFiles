set PATH $PATH /opt/Programs/4kvideodownloader
set PATH $PATH $HOME/.cargo/bin
set GOROOT /usr/lib/go
set GOPATH $HOME/go
set PATH $GOPATH/bin $GOROOT/bin $PATH
set PATH /home/christopher/.local/bin $PATH
set PATH /home/christopher/bin $PATH
set EDITOR vim
fish_vi_key_bindings
eval (starship init fish)
