set nocompatible              " be iMproved, required
inoremap jk <ESC>
" shows a menu when using tab completion
set wildmenu

" search ignoring case unless you find a capital
set ignorecase
set smartcase

" This sets automatic indentation with smart indentation
set ai
set si
set bg=dark

set hidden

let mapleader = " "
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
filetype off
call plug#begin('~/.vim/plugged')

if has('gui_running')
  set background=dark
  colorscheme slate
  set guifont=Hack\ Nerd\ Font\ Mono\ Regular\ 15    
else 
  set background=dark
  colorscheme slate
endif
set is
set hls
" " The following are examples of different formats supported.
" " Keep Plug commands between plug#begin/end.
Plug 'ervandew/supertab'
Plug 'tenfyzhong/CompleteParameter.vim'
Plug 'SirVer/ultisnips'
Plug 'fatih/vim-go'
Plug 'ycm-core/YouCompleteMe'
Plug 'nim-lang/nimble'
Plug 'baabelfish/nvim-nim'
Plug 'kana/vim-operator-user'
Plug 'nim-lang/nimsuggest'
Plug 'junegunn/vim-plug' 
Plug 'tpope/vim-fugitive' 
Plug 'sebdah/vim-delve'
Plug 'tpope/vim-surround'
Plug 'craigemery/vim-autotag' 
Plug 'scrooloose/nerdtree' 
Plug 'rust-lang/rust.vim' 
Plug 'xolox/vim-misc' 
Plug 'gberenfield/dotvim' 
Plug 'chrisfair/vim-easytags' 
Plug 'Shougo/deoplete.nvim'
Plug 'racer-rust/vim-racer'
Plug 'Shougo/neocomplcache.vim'  
Plug 'tpope/vim-rails'  
Plug 'tpope/vim-markdown'  
Plug 'christoomey/vim-tmux-navigator'
Plug 'elzr/vim-json'  
Plug 'honza/vim-snippets'
Plug 'tomtom/tinykeymap_vim'  
Plug 'uarun/vim-protobuf'  
Plug 'majutsushi/tagbar'  
Plug 'chrisbra/csv.vim'  
Plug 'govim/govim'
Plug 'vim-jp/vim-go-extra'
Plug 'kergoth/vim-bitbake'  
Plug 'timonv/vim-cargo' 
Plug 'jansenm/vim-cmake'  
Plug 'jalcine/cmake.vim'  
Plug 'pboettch/vim-cmake-syntax' 
Plug 'moll/vim-node' 
Plug 'tmhedberg/SimpylFold' 
Plug 'vim-scripts/indentpython.vim' 
Plug 'vim-syntastic/syntastic' 
Plug 'nvie/vim-flake8' 
Plug 'jnurmine/Zenburn' 
Plug 'altercation/vim-colors-solarized' 
Plug 'ryanoasis/vim-devicons'
Plug 'mikelue/vim-maven-plugin'
Plug 'powerline/powerline'
Plug 'thindil/vim-ada'
Plug 'Chiel92/vim-autoformat'
Plug 'brookhong/cscope.vim'
Plug 'ollykel/v-vim'
Plug 'ryanoasis/vim-devicons'
Plug 'calviken/vim-gdscript3'
Plug 'JuliaEditorSupport/julia-vim'
Plug 'zyedidia/julialint.vim'

" " All of your Plugins must be added before the following line
call plug#end()            " required
" filetype plugin indent on    " required
" " To ignore plugin indent changes, instead use:
filetype plugin on
" "
" " Brief help
" " :PlugList       - lists configured plugins
" " :PlugInstall    - installs plugins; append `!` to update or just :PluginUpdate
" " :PlugSearch foo - searches for foo; append `!` to refresh local cache
" " :PlugClean      - confirms removal of unused plugins; append `!` to auto-approve removal
" "
" " see :h vim-plug for more details or wiki for FAQ






" These are the mappings for cscope
" s: Find this C symbol
nnoremap <leader>fa :call CscopeFindInteractive(expand('<cword>'))<CR>
nnoremap <leader>l :call ToggleLocationList()<CR>

nnoremap  <leader>fs :call CscopeFind('s', expand('<cword>'))<CR>
" " g: Find this definition
nnoremap  <leader>fg :call CscopeFind('g', expand('<cword>'))<CR>
" " d: Find functions called by this function
nnoremap  <leader>fd :call CscopeFind('d', expand('<cword>'))<CR>
" " c: Find functions calling this function
nnoremap  <leader>fc :call CscopeFind('c', expand('<cword>'))<CR>
" " t: Find this text string
nnoremap  <leader>ft :call CscopeFind('t', expand('<cword>'))<CR>
" " e: Find this egrep pattern
nnoremap  <leader>fe :call CscopeFind('e', expand('<cword>'))<CR>
" " f: Find this file
nnoremap  <leader>ff :call CscopeFind('f', expand('<cword>'))<CR>
" " i: Find files #including this file
nnoremap  <leader>fi :call CscopeFind('i', expand('<cword>'))<CR>
noremap! <F12> ,t strftime("%H:%M")

set rtp+=$HOME/.local/lib/python2.7/site-packages/powerline/bindings/vim/


" Always show statusline
"
set laststatus=2
"
"
"
" " Use 256 colours (Use this setting only if your terminal supports 256 colours)
"
set t_Co=256

""
filetype plugin indent on
"
autocmd FileType vim let b:vcm_tab_complete = 'vim'

set ruler
set number
set smarttab
set fileformats=unix,dos,mac " support all three, in this order
set formatoptions=tcqor " t=text, c=comments, q=format with "gq", o,r=autoinsert comment leader
set cindent " indent on cinwords set shiftwidth=2                " set shiftwidth to 5 spaces" 
set tabstop=2                   " set tab to 2 spaces
set showmatch                   " Show matching brackets/braces/parantheses.
set background=dark     " set background to dark
set showcmd                             " Show (partial) command in status line.
set autowrite                   " Automatically save before commands like :next and :make
set textwidth=98                " My terminal is 98 characters wide
set visualbell                          " Silence the bell, use a flash instead
set cinoptions=:.5s,>1s,p0,t0,(0,g2     " :.5s = indent case statements 1/2 shiftwidth
set cinwords=if,else,while,do,for,switch,case,class,try   " Which keywords should indent
set showmatch
set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [ASCII=\%03.3b]\ [HEX=\%02.2B]\ [POS=%02l,%02v]\ [%p%%]\ [LEN=%L] "Shows detailed status line with formatting
set laststatus=2 "This Makes the status bar visible
set mat=5
set tabstop=2 shiftwidth=2 expandtab
filetype plugin on
filetype indent on
set modeline
set mouse=a
set nocompatible
"set cursorline
"highlight CursorLine guibg=lightblue ctermbg=lightgray
"set cursorcolumn

" vimrc file for following the coding standards specified in PEP 7 & 8.
"
" To use this file, source it in your own personal .vimrc file (``source
" <filename>``) or, if you don't have a .vimrc file, you can just symlink to it
" (``ln -s <this file> ~/.vimrc``).  All options are protected by autocmds
" (read below for an explanation of the command) so blind sourcing of this file
" is safe and will not affect your settings for non-Python or non-C files.
"
"
" All setting are protected by 'au' ('autocmd') statements.  Only files ending
" in .py or .pyw will trigger the Python settings while files ending in *.c or
" *.h will trigger the C settings.  This makes the file "safe" in terms of only
" adjusting settings for Python and C files.
"
" Only basic settings needed to enforce the style guidelines are set.
" Some suggested options are listed but commented out at the end of this file.

" Number of spaces that a pre-existing tab is equal to.
" For the amount of space used for a new tab use shiftwidth.
au BufRead,BufNewFile *py,*pyw,*.c,*.h,*.pl,*.pm  set tabstop=5

" What to use for an indent.
" This will affect Ctrl-T and 'autoindent'.
" Python and PHP: 5 spaces
" C and perl : tabs (pre-existing files) or 4 spaces (new files)
au BufRead,BufNewFile *.py,*pyw,*.php set shiftwidth=4
au BufRead,BufNewFile *.py,*pyw,*.php set softtabstop=4
au BufRead,BufNewFile *.py,*pyw,*.php set tabstop=4
au BufRead,BufNewFile *.py,*pyw,*.php set textwidth=79
au BufRead,BufNewFile *.py,*pyw,*.php set expandtab
au BufRead,BufNewFile *.py,*pyw,*.php set autoindent
au BufRead,BufNewFile *.py,*pyw,*.php set fileformat=unix
au BufRead,BufNewFile *.py,*.pyw,*.php set expandtab

" These are the bufreads for rust-tags
autocmd BufRead *.rs :setlocal tags=./rusty_tags.vi;/
autocmd BufWrite *.rs :silent! exec "!rusty_tags vi --quite --start-dir=" .expand(%:p:h') . "&"
autocmd BufRead *.rs :setlocal  tags=./.rusty_tags.vi;/,$RUST_SRC_PATH/rusty-tags.vi 


fu Select_c_style()
  if search('^\t', 'n', 150)
    set shiftwidth=5
    set noexpandtab
  el 
    set shiftwidth=5
    set expandtab
  en
endf

au BufRead,BufNewFile *.c,*.h,*.pl,*.pm,*.php call Select_c_style()
au BufRead,BufNewFile Makefile* set noexpandtab

" Use the below highlight group when displaying bad whitespace is desired.
highlight BadWhitespace ctermbg=red guibg=red

" Display tabs at the beginning of a line in Python mode as bad.
au BufRead,BufNewFile *.py,*.pyw match BadWhitespace /^\t\+/
" Make trailing whitespace be flagged as bad.
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h,*.pl,*.pm,*.php match BadWhitespace /\s\+$/

" Wrap text after a certain number of characters
" Python: 79 
" C: 79
" Perl: 79
" PHP: 79
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h,*.pl,*.pm,*.php set textwidth=79

" Turn off settings in 'formatoptions' relating to comment formatting.
" - c : do not automatically insert the comment leader when wrapping based on
"    'textwidth'
" - o : do not insert the comment leader when using 'o' or 'O' from command mode
" - r : do not insert the comment leader when hitting <Enter> in insert mode
" Python and Perl: not needed
" C: prevents insertion of '*' at the beginning of every line in a comment
au BufRead,BufNewFile *.c,*.h set formatoptions-=c formatoptions-=o formatoptions-=r

" Use UNIX (\n) line endings.
" Only used for new files so as to not force existing files to change their
" line endings.
" Python: yes
" C: yes
" Perl: yes
au BufNewFile *.py,*.pyw,*.c,*.h,*.pm,*.php set fileformat=unix

" These commands set up the program to read a binary file as a hex file
augroup Binary
  autocmd!
  autocmd BufReadPre *.bin let &bin=1
  autocmd BufReadPost *.bin if &bin | %!xxd
  autocmd BufReadPost *.bin set ft=xxd | endif
  autocmd BufWritePre *.bin if &bin | %!xxd -r
  autocmd BufWritePre *.bin endif
  autocmd BufWritePost *.bin if &bin | %!xxd
  autocmd BufWritePost *.bin set nomod | endif
augroup END

set encoding=UTF-8

" ----------------------------------------------------------------------------
" The following section contains suggested settings.  While in no way required
" to meet coding standards, they are helpful.

" Set the default file encoding to UTF-8: ``set encoding=utf-8``

" Puts a marker at the beginning of the file to differentiate between UTF and
" UCS encoding (WARNING: can trick shells into thinking a text file is actually
" a binary file when executing the text file): ``set bomb``

" For full syntax highlighting:
let python_highlight_all=1

syntax on``

" Automatically indent based on file type: ``filetype indent on``
" Keep indentation level from previous line: ``set autoindent``

" Folding based on indentation: ``set foldmethod=indent``

" Show tabs and trailing spaces.
" Ctrl-K >> for »
" Ctrl-K .M for ·
" (use :dig for list of digraphs)

" my perl includes pod
let perl_include_pod = 1
" syntax color complex things like @{${"foo"}}
let perl_extended_vars = 1

" Ultisnips
let g:UltiSnipsSnippetsDir = $HOME.'/.vim/plugged/vim-snippets/UltiSnips'
let g:UltiSnipsSnippedDirectories=['UltiSnips']
let g:UltiSnipsExpandTrigger='<c-j>'
let g:UltiSnipsListSnippets='<c-h>'
let g:UltiSnipsJumpForwardTrigger='<c-j>'
let g:UltiSnipsJumpBackwardTrigger='<c-k>'

" You completeme
" make TCM compatible with UltiSnips (using supertab)
let g:ycm_complete_in_comments = 1
let g:ycm_seed_identifiers_with_syntax = 1
let g:ycm_collect_identifiers_from__comments_and_strings = 1
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'


"set foldmethod=indent
nmap <F2> 0v%zf

syntax on
filetype plugin indent on

" Change backup directory to a standard one
set backupdir=~/.vimtmp
set directory=~/.vimtmp

"Configuration for pydiction
let g:pydiction_location = '~/.vim/bundle/pydiction/complete-dict'

" These are my mappings of my function keys
nmap <F5> :NERDTreeToggle<CR>
nmap <F6> :TagbarToggle<CR>
nmap <F8> :set list!<CR>
" These allow me to automatically run NERDTree when opening vim
" autocmd VimEnter * TlistOpen
" autocmd VimEnter * NERDTree 
map <leader>gt:call TimeLapse() <CR>
let g:zipPlugin_ext = '*.zip,*.jar,*.xpi,*.ja,*.war,*.ear,*.celzip,*.oxt,*.kmz,*.wsz,*.xap,*.docx,*.docm,*.dotx,*.dotm,*.potx,*.potm,*.ppsx,*.ppsm,*.pptx,*.pptm,*.ppam,*.sldx,*.thmx,*.crtx,*.vdw,*.glox,*.gcsx,*.gqsx'

let g:tagbar_type_go = {
      \ 'ctagstype' : 'go',
      \ 'kinds'     : [
        \ 'p:package',
        \ 'i:imports:1',
        \ 'c:constants',
        \ 'v:variables',
        \ 't:types',
        \ 'n:interfaces',
        \ 'w:fields',
        \ 'e:embedded',
        \ 'm:methods',
        \ 'r:constructor',
        \ 'f:functions'
        \ ],
        \ 'sro' : '.',
        \ 'kind2scope' : {
          \ 't' : 'ctype',
          \ 'n' : 'ntype'
          \ },
          \ 'scope2kind' : {
            \ 'ctype' : 't',
            \ 'ntype' : 'n'
            \ },
            \ 'ctagsbin'  : 'gotags',
            \ 'ctagsargs' : '-sort -silent'
            \ }

let g:tagbar_type_rust = {
      \ 'ctagstype' : 'rust',
      \ 'kinds' : [
        \'T:types,type definitions',
        \'f:functions,function definitions',
        \'g:enum,enumeration names',
        \'s:structure names',
        \'m:modules,module names',
        \'c:consts,static constants',
        \'t:traits,traits',
        \'i:impls,trait implementations',
        \]
        \}
let g:powerline_pycmd="py3"
set rtp+=/usr/lib/python3.8/site-packages/powerline/bindings/vim
let g:clang_library_path='/usr/lib64/libclang.so'
let g:ycm_global_ycm_extra_conf = '.vim/bundle/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py'
let g:ycm_rust_src_path = $RUST_SRC_PATH
let g:rustfmt_autosave = 1

" Remap move to splits to more logical options 
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Setup folding
set foldmethod=indent
set foldlevel=99
nnoremap <space> za
let g:SimpylFold_docstring_preview=1
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g    :YcmCompleter GoToDefinitionsElseDeclaration<CR>

let python_highlight_all=1
syntax on
" ignore files in NERDTree that end in .pyc
let NERDTreeIgnore=['\.pyc$', '\~-$']

" allow toggle between solarized theme
call togglebg#map("<F4>")

set clipboard=unnamedplus



